# API-Acteur

## Objectif 
* Informations sur les acteur : 
    * Films 
    * Emissions TV, series ...
    * Récompenses (Oscar, César, Golden Globe ...)
    * Nominations (Oscar, César, Golden Globe ...)
    * Informations personnel (date de naissance etc)
    * Recherche associée (autres acteur avec qui ils ont joué récement)
